# vallode-com

My personal little space on the internet. Currently hosted using
[Netlify](https://netlify.com/) but aiming to figure out self-hosting
at some point. This documentation is mostly aimed at myself, but it might help others who want to
create a website similar to this one :)

## Development

Single dependency: [Zola](https://www.getzola.org/).

Run locally with:

```
zola serve
```
