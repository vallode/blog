+++
+++

Hey, my name is Mariusz or Vallode, depending on where you know me from :)

I'm a web developer, a bit of a handyman. I am focused on UX right now, trying to supplement my
knowledge of frontend and backend programming with the empathy designers have for users.

I'm a **big fan** of the small (not _necessarily_ minimal) web, feel free to explore this plot of
digital land that I call my homepage.

## External links

As most people my presence on the internet is vast (annoyingly so), you can find me active on a few
different websites:

* Gitlab [@vallode](https://gitlab.com/vallode)
* Github [@vallode](https://github.com/vallode)
* Twitter [@Vallode](https://twitter.com/Vallode_)
