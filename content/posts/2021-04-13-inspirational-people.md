+++
title = "Inspirational people"
date = 2021-04-13
updated = 2021-04-13
+++

A good friend of mine recently shared a list of people he finds inspirational,
I was then inspired by said list to make my own and try to keep it up-to-date.

* Boyan Slat
* Alan Watts

I want to dedicate a section to each of them, perhaps as an internal conversation
as to why I find them inspiring. (I know the list is small, give me some time!)

## Boyan Slat

Boyan Slat is a name that I want to see mentioned in my circles more often, the
story goes that aged 16 Boyan was diving in Greece (very fancy) but came across
more plastic than fish there, fast-forward until now, and he has devoted himself
almost wholly into trying to clean up the oceans across the world.

As far as inspiration is concerned, every time I read about his endeavours I hope
to be half as driven as he is.

## Alan Watts

I learned about Alan Watts while sleeping on a roughly put together bed made of
two pallets, an uncle of a friend played one of his "lectures" and I was
entranced by the determined voice of Alan Watts.

While I may not agree with everything Alan Watts says and his personal life has
seen more than it's share of troubles (most seemingly caused by him), I do
think his speeches give me a form of inspiration. 

"You're it" is a great speech by him, very short too, which I'd highly
recommend to anyone.



