+++
title = "Posts"
sort_by = "date"
+++

I enjoy writing but I'm woefully bad at publishing the pieces, I want to get better and so this is a
collection of all my writing.
