+++
title = "Startpages"
date = 2021-03-12
updated = 2021-03-12
+++

One of the first hobbies that really and truly drove me to persue web development was the creation
of _startpages_.

A startpage is the website that appears when you press new tab, you might have that
page be set to a search engine, or you might have let your browser keep it's page with news and
other widgets. All of these pages, so long as they open each and every time you click "new tab", are
**startpages**!

## Why I love startpages

Startpages are a piece of the old web, the community involved in them holds a very strong bastion of
hope that we are still in control of the things we see each day. Given a little tech know-how and a
tutorial you can _create from scratch_ the **most visited website in your browser**.

Isn't that wild?

My current startpage is little more than a few links pointing me to websites I like and projects I
am working on, complexity isn't the appeal here. It's the fact that the thing I see every time I
open a new tab was created by me... for me!

The idea that you are in direct control over the thing you see on your screen is great, it's the
highest peak of user control and customizatibility.

Yet browsers are consistently trying to kill it, with every update Firefox and Chrome are making it
harder and harder to customise the browser readily and easily.

## More info

If you'd like to discover a little more about startpages, you can visit a site I run:
[stpg.tk](https://stpg.tk/) or check out
[the reddit community](https://www.reddit.com/r/startpages/)!
